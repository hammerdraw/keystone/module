from hammerdraw.compilers import CompilerBase
from hammerdraw.modules.core import ImageModule, TextModule

class KeystoneCardCompiler(CompilerBase):
    modules = \
    [
        ImageModule,
        (TextModule,  dict(name='name', scale_field='titleFontSizeScale')),
        (TextModule,  dict(name='type', scale_field='typeFontSizeScale')),
        (TextModule,  dict(name='effect', scale_field='textFontSizeScale')),
        (TextModule,  dict(name='mineralsShade', field='minerals', convert_to_str=True)),
        (TextModule,  dict(name='minerals', convert_to_str=True)),
        (TextModule,  dict(name='gasShade', field='gas', convert_to_str=True)),
        (TextModule,  dict(name='gas', convert_to_str=True)),
        (ImageModule, dict(name='set', path_from_config=True, format_path=True, foreground=True)),
        (ImageModule, dict(name='rarity', path_from_config=True, format_path=True, foreground=True)),
    ]
    module_name = 'keystone'
    compiler_type = "keystone-card"
    
    def _get_base_filename(self) -> str:
        return super()._get_base_filename().format(**self.raw)
    def get_output_directory(self) -> str:
        _raw = dict(self.raw)
        _set = _raw['set']
        _race = _raw['race']
        _set_race = _set if ('/' in _set) else f'{_set}/{_race}'
        _raw.setdefault('setRace', _set_race)
        return self.output_directory.format(**_raw)
